﻿namespace FirstApp;

public partial class MainPage : ContentPage
{
	int count = 0;
	string myText = "";
    string myText2 = "";
    List<string> nombresList = new List<string>();


    public MainPage()
	{
		InitializeComponent();
	}

	private void OnTestClicked(object sender, EventArgs e)
	{
		
		
            TestBtn.Text = $"Name \"{myText}\" entered succesfully!";
			nombresList.Add(myText);
			
       

		SemanticScreenReader.Announce(TestBtn.Text);
	}

    void OnEntryTextChanged(object sender, TextChangedEventArgs e)
    {
        myText = entry.Text;
    }

    private void ShowLista(object sender, EventArgs e)
    {

        myText = "";
        foreach (string a in nombresList)
        {
            myText += "Name: " + a + ", ";
        }

        ShowListBtn.Text = myText;

        SemanticScreenReader.Announce(ShowListBtn.Text);
    }

    void OnEntryTextChanged1(object sender, TextChangedEventArgs e)
    {
        myText = entry1.Text;
    }

    void OnEntryTextChanged2(object sender, TextChangedEventArgs e)
    {
        myText2 = entry2.Text;
    }

    private void UpdateClicked(object sender, EventArgs e)
    {
        for (int i = 0; i < nombresList.Count; i++)
        {
            if (nombresList[i] == myText)
            {
                nombresList[i] = myText2;
            }
        }


        updateBtn.Text = $"Updated name \"{myText}\" to name \"{myText2}\"";
        SemanticScreenReader.Announce(updateBtn.Text);
    }


    void OnEntryTextChanged3(object sender, TextChangedEventArgs e)
    {
        myText = "";
        myText = entry3.Text;
    }

    private void DeleteClicked(object sender, EventArgs e)
    {
        
        for (int i = 0; i < nombresList.Count; i++)
        {
            if (nombresList[i] == myText)
            {
                nombresList.RemoveAt(i);
            }
        }

        deleteBtn.Text = $"deleted name \"{myText}\"!";
        SemanticScreenReader.Announce(deleteBtn.Text);
    }

}


